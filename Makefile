## script running Fast Downward planner in a bit more convinient way
## clone FD repo and build it according to following instruction:
## http://www.cs.put.poznan.pl/ekowalczuk/pddlLinuxConsole.pdf
## adjust FAST_DOWNWARD_PATH
FAST_DOWNWARD_PATH = ../../tools/fastDownward/fast-downward.py
## for Sublime Text there is myPDDL plugin: https://github.com/Pold87/myPDDL
## project is compatible with it after you finish setup

PYTHON = python
FAST_DOWNWARD_CALL = $(PYTHON) $(FAST_DOWNWARD_PATH)

DOMAIN_NAME = domain
DOMAIN = $(DOMAIN_NAME).pddl

PROBLEMS_PATH = problems
DEF_PROBLEM_NAME = problem_susm
DEF_PROBLEM = $(PROBLEMS_PATH)/$(DEF_PROBLEM_NAME)

SOLUTION_DIR = solutions
SOLUTION_NAME = sas_plan
SOLUTION = $(SOLUTION_DIR)/$(SOLUTION_NAME)

PROCESS_MEMORY_LIMIT = 2000000


plan: sas_plan

move_output:
	mv sas_plan $(SOLUTION_DIR)/
	mv output $(SOLUTION_DIR)/
	mv output.sas $(SOLUTION_DIR)/

## calculate plan
sas_plan: call_planner move_output

## show plan
show_plan: sas_plan
	less $(SOLUTION)
show: show_plan

## PDDL fast downward: A* algorithm, blind heuristics
%.pddl:
	$(FAST_DOWNWARD_CALL) $(DOMAIN) $@ --search "astar(blind())"

call_planner:
	$(FAST_DOWNWARD_CALL) $(DOMAIN) $(DEF_PROBLEM).pddl --search "astar(blind())"


.PHONY = clean call_planner move_output plan sas_plan show show_plan

clean:
	rm $(SOLUTION_DIR)/sas_plan $(SOLUTION_DIR)/output $(SOLUTION_DIR)/output.sas
