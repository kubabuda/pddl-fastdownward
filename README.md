# Sample PDDL Project

Most basic- World of blocks

With some scripts running Fast Downward planner from terminal in a bit more convinient way

# Setup

* instal dependencies:
```bash
    sudo apt-get install mercurial g++ make python flex bison g++-multilib
```

* clone fD repo:
```bash
    hg clone http://hg.fast-downward.org fastDownward
```

* build fD, with ```DOWNWARD_BITWIDTH=64``` on 64bit machine:
```bash
    cd fastDownward/src
    python build_all.py DOWNWARD_BITWIDTH=64
```

* adjust ```FAST_DOWNWARD_PATH``` in Makefile, or ```fast_downward_path``` in ```plan``` shell script, up to taste

* for Sublime Text there is myPDDL plugin: https://github.com/Pold87/myPDDL, follow its setup instruction

# Running

* You might want to set memory limit for process:
```bash
ulimit -v 2000000
```

* Then run ```make plan``` or ```./plan```

* or ```make show``` to see the calculated plan, and exit ```less``` with ```q``` key

gl&hf
