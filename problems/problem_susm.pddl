;Susman anomaly
(define (problem p1)
    (:domain world-of-blocks)
    (:objects a b c)
    (:init
        (clear c)
        (clear b)
        (armempty)
        (on-top c a)
        (on-floor a)
        (on-floor b)
    )
    (:goal
        (and
            (clear a)
            (on-top a b)
            (on-top b c)
            (on-floor c)
        )
    )
)
